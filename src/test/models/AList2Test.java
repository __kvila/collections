package test.models;

import main.java.com.collections.models.AList2;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;

public class AList2Test {

    private AList2 cut = new AList2();

    @Test
    void clearTest() {

        String [] testArray = new String[] {"a", "b", "c"};

        cut = new AList2(testArray);

        boolean expected = true;

        cut.clear();

        boolean actual = true;

        for (int i = 0; i < cut.size(); i++) {

            if (cut.get(i) != null) {

                actual = false;

            }

        }

        Assertions.assertEquals(expected, actual);
    }

    static List<Arguments> sizeTestArgs() {
        return List.of(
                Arguments.arguments(10),
                Arguments.arguments(5),
                Arguments.arguments(-1)
        );
    }

    @ParameterizedTest
    @MethodSource("sizeTestArgs")
    void sizeTest(int expected) {

        int actual = cut.size();

        Assertions.assertEquals(expected, actual);

    }

    static List<Arguments> getTestArgs() {
        return List.of(
                Arguments.arguments("a", 0, "a"),
                Arguments.arguments("a", 0, "b"),
                Arguments.arguments("a", 1, null)
        );
    }

    @ParameterizedTest
    @MethodSource("getTestArgs")
    void getTest(String testElement, int index, String expected) {

        String [] testArray = new String[] {testElement};

        cut = new AList2(testArray);

        String actual = (String) cut.get(index);

        Assertions.assertEquals(expected, actual);

    }

    static List<Arguments> addTestArgs() {
        return List.of(
                Arguments.arguments("a", "a"),
                Arguments.arguments("b", "a")
        );
    }

    @ParameterizedTest
    @MethodSource("addTestArgs")
    void addTest(String value, String expected) {

        cut.add(value);

        String actual = (String) cut.get(cut.size() - 1);

        Assertions.assertEquals(expected, actual);

    }

    static List<Arguments> addByIndexTestArgs() {
        return List.of(
                Arguments.arguments(5, "a", true),
                Arguments.arguments(10, "a", false),
                Arguments.arguments(-1, "a", false)
        );
    }

    @ParameterizedTest
    @MethodSource("addByIndexTestArgs")
    void addByIndexTest(int index, String testElement, boolean expected) {

        boolean actual = cut.add(index, testElement);

        Assertions.assertEquals(expected, actual);

    }

    static List<Arguments> removeTestArgs() {
        return List.of(
                Arguments.arguments(new String [] {"a", "b", "c"}, "b", 1),
                Arguments.arguments(new String [] {"a", "b", "c"}, "d", 0)
        );
    }

    @ParameterizedTest
    @MethodSource("removeTestArgs")
    void removeTest(String [] testArray, String removeValue, int expected) {

        cut = new AList2(testArray);

        int actual = cut.remove(removeValue);

        Assertions.assertEquals(expected, actual);

    }

    static List<Arguments> removeByIndexTestArgs() {
        return List.of(
                Arguments.arguments(new String [] {"a", "b", "c"}, 0, 1),
                Arguments.arguments(new String [] {"a", "b", "c"}, 3, 0),
                Arguments.arguments(new String [] {"a", "b", "c"}, -1, 0)
        );
    }

    @ParameterizedTest
    @MethodSource("removeByIndexTestArgs")
    void removeByIndexTest(String [] testArray, int removeIndex, int expected) {

        cut = new AList2(testArray);

        int actual = cut.removeByIndex(removeIndex);

        Assertions.assertEquals(expected, actual);

    }

    static List<Arguments> containsTestArgs() {
        return List.of(
                Arguments.arguments(new String [] {"a", "b", "c"}, "b", true),
                Arguments.arguments(new String [] {"a", "b", "c"}, "d", false)
        );
    }

    @ParameterizedTest
    @MethodSource("containsTestArgs")
    void containsTest(String [] testArray, String value, boolean expected) {

        cut = new AList2(testArray);

        boolean actual = cut.contains(value);

        Assertions.assertEquals(expected, actual);

    }

    static List<Arguments> setTestArgs() {
        return List.of(
                Arguments.arguments(1, "a", true),
                Arguments.arguments(9, "h", true),
                Arguments.arguments(10, "j", false),
                Arguments.arguments(-1, "z", false)
        );
    }

    @ParameterizedTest
    @MethodSource("setTestArgs")
    void setTest(int index, String value, boolean expected) {

        boolean actual = cut.set(index, value);

        Assertions.assertEquals(expected, actual);

    }

    static List<Arguments> toArrayTestArgs() {
        return List.of(
                Arguments.arguments(new String [] {"a", "b", "c"}, new String [] {"a", "b", "c"}, true),
                Arguments.arguments(new String [] {"c", "b", "a"}, new String [] {"a", "b", "c"}, false)
        );
    }

    @ParameterizedTest
    @MethodSource("toArrayTestArgs")
    void toArrayTest(String [] testArray , String [] expectedArray, boolean expected) {

        cut = new AList2(testArray);

        boolean actual = true;

        for (int i = 0; i < cut.size(); i++) {

            if (!cut.get(i).equals(expectedArray[i])) {

                actual = false;

            }

        }

        Assertions.assertEquals(expected, actual);

    }

    @Test
    void removeAllTest() {

        boolean expected = true;

        cut = new AList2(new String[] {"a", "b", "c", "d", "e"});

        boolean actual = cut.removeAll();

        Assertions.assertEquals(expected, actual);

    }

}

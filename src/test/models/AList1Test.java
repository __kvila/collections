package test.models;

import main.java.com.collections.models.AList1;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.util.List;

public class AList1Test {

    private AList1 cut = new AList1();

    @Test
    void clearTest() {

        int [] testArray = new int[] {1, 2, 3};

        cut = new AList1(testArray);

        int expected = 0;

        cut.clear();

        int actual = cut.get(0) + cut.get(1) + cut.get(2);

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void sizeTest() {

        int expected = 10;

        int actual = cut.size();

        Assertions.assertEquals(expected, actual);

    }

    static List<Arguments> getTestArgs() {
        return List.of(
                Arguments.arguments(1, 1),
                Arguments.arguments(3, 1)
        );
    }

    @ParameterizedTest
    @MethodSource("getTestArgs")
    void getTest(int testElement, int expected) {

        int [] testArray = new int[] {testElement};

        cut = new AList1(testArray);

        int actual = cut.get(0);

        Assertions.assertEquals(expected, actual);

    }

    static List<Arguments> addTestArgs() {
        return List.of(
                Arguments.arguments(1, 1),
                Arguments.arguments(3, 1)
        );
    }

    @ParameterizedTest
    @MethodSource("addTestArgs")
    void addTest(int testElement, int expected) {

        int prevSize = cut.size();

        cut.add(testElement);

        int actual = cut.get(prevSize);

        Assertions.assertEquals(expected, actual);

    }

    static List<Arguments> addByIndexTestArgs() {
        return List.of(
                Arguments.arguments(1, 1, 1),
                Arguments.arguments(1, 3, 1)
        );
    }

    @ParameterizedTest
    @MethodSource("addByIndexTestArgs")
    void addByIndexTest(int index, int testElement, int expected) {

        cut.add(index, testElement);

        int actual = cut.get(index);

        Assertions.assertEquals(expected, actual);

    }

    static List<Arguments> removeTestArgs() {
        return List.of(
                Arguments.arguments(new int [] {1, 2, 3}, 2, 1),
                Arguments.arguments(new int [] {1, 2, 3}, 4, 0),
                Arguments.arguments(new int [] {1, 2, 3}, 4, 1)
        );
    }

    @ParameterizedTest
    @MethodSource("removeTestArgs")
    void removeTest(int [] testArray, int removeValue, int expected) {

        cut = new AList1(testArray);

        int actual = cut.remove(removeValue);

        Assertions.assertEquals(expected, actual);

    }

    static List<Arguments> removeByIndexTestArgs() {
        return List.of(
                Arguments.arguments(new int [] {1, 2, 3}, 1, 1),
                Arguments.arguments(new int [] {1, 2, 3}, 3, 0),
                Arguments.arguments(new int [] {1, 2, 3}, -1, 0)
        );
    }

    @ParameterizedTest
    @MethodSource("removeByIndexTestArgs")
    void removeByIndexTest(int [] testArray, int removeIndex, int expected) {

        cut = new AList1(testArray);

        int actual = cut.removeByIndex(removeIndex);

        Assertions.assertEquals(expected, actual);

    }

    static List<Arguments> containsTestArgs() {
        return List.of(
                Arguments.arguments(new int [] {1, 2, 3}, 1, true),
                Arguments.arguments(new int [] {1, 2, 3}, 4, false)
        );
    }

    @ParameterizedTest
    @MethodSource("containsTestArgs")
    void containsTest(int [] testArray, int value, boolean expected) {

        cut = new AList1(testArray);

        boolean actual = cut.contains(value);

        Assertions.assertEquals(expected, actual);

    }

    static List<Arguments> setTestArgs() {
        return List.of(
                Arguments.arguments(1, 5, true),
                Arguments.arguments(9, 5, true),
                Arguments.arguments(10, 5, false),
                Arguments.arguments(-1, 5, false)
        );
    }

    @ParameterizedTest
    @MethodSource("setTestArgs")
    void setTest(int index, int value, boolean expected) {

        boolean actual = cut.set(index, value);

        Assertions.assertEquals(expected, actual);

    }

    static List<Arguments> toArrayTestArgs() {
        return List.of(
                Arguments.arguments(new int [] {1, 2, 3}, new int[] {1, 2, 3}, true),
                Arguments.arguments(new int [] {3, 2, 1}, new int[] {1, 2, 3}, false)
        );
    }

    @ParameterizedTest
    @MethodSource("toArrayTestArgs")
    void toArrayTest(int [] testArray , int [] expectedArray, boolean expected) {

        cut = new AList1(testArray);

        boolean actual = true;

        for (int i = 0; i < cut.size(); i++) {

            if (cut.get(i) != expectedArray[i]) {

                actual = false;

            }

        }

        Assertions.assertEquals(expected, actual);

    }

    @Test
    void removeAllTest() {

        boolean expected = true;

        int [] testArray = new int[] {1, 2, 3, 4, 5};

        boolean actual = cut.removeAll(testArray);

        Assertions.assertEquals(expected, actual);

    }

}

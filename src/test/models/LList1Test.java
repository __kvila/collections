package test.models;

import main.java.com.collections.models.LList1;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import java.util.List;

public class LList1Test {

    private LList1 cut = new LList1(1);

    static List<Arguments> sizeTestArgs() {
        return List.of(
                Arguments.arguments(10),
                Arguments.arguments(5),
                Arguments.arguments(-1)
        );
    }

    @ParameterizedTest
    @MethodSource("sizeTestArgs")
    void sizeTest(int expected) {

        int actual = cut.size();

        Assertions.assertEquals(expected, actual);

    }

    static List<Arguments> getByIndexTestArgs() {
        return List.of(
                Arguments.arguments("a", 1, "a"),
                Arguments.arguments("a", 2, null),
                Arguments.arguments("a", 0, null)
        );
    }

    @ParameterizedTest
    @MethodSource("getByIndexTestArgs")
    void getByIndexTest(String testValue, int index, String expected) {

        cut.set(1, testValue);

        String actual = (String) cut.get(index);

        Assertions.assertEquals(expected, actual);

    }

    static List<Arguments> addTestArgs() {
        return List.of(
                Arguments.arguments("a", "a"),
                Arguments.arguments("b", "a")
        );
    }

    @ParameterizedTest
    @MethodSource("addTestArgs")
    void addTest(String value, String expected) {

        cut.add(value);

        String actual = (String) cut.get(cut.size());

        Assertions.assertEquals(expected, actual);

    }

    static List<Arguments> addByIndexTestArgs() {
        return List.of(
                Arguments.arguments(1, "a", true),
                Arguments.arguments(2, "a", false),
                Arguments.arguments(0, "a", false),
                Arguments.arguments(-1, "a", false)
        );
    }

    @ParameterizedTest
    @MethodSource("addByIndexTestArgs")
    void addByIndexTest(int index, String testElement, boolean expected) {

        boolean actual = cut.add(index, testElement);

        Assertions.assertEquals(expected, actual);

    }

    static List<Arguments> removeTestArgs() {
        return List.of(
                Arguments.arguments("a", 1),
                Arguments.arguments("b", 0)
        );
    }

    @ParameterizedTest
    @MethodSource("removeTestArgs")
    void removeTest(String value, int expected) {

        cut.set(1, "a");

        int actual = cut.remove(value);

        Assertions.assertEquals(expected, actual);

    }

    @Test
    void removeIfEmpty() {

        int expected = 0;

        cut.clear();

        int actual = cut.remove("a");

        Assertions.assertEquals(expected, actual);

    }

    static List<Arguments> removeByIndexTestArgs() {
        return List.of(
                Arguments.arguments(1, 1),
                Arguments.arguments(0, 0),
                Arguments.arguments(-1, 0)
        );
    }

    @ParameterizedTest
    @MethodSource("removeByIndexTestArgs")
    void removeByIndexTest(int index, int expected) {

        cut.set(1, "a");

        int actual = cut.removeByIndex(index);

        Assertions.assertEquals(expected, actual);

    }

    static List<Arguments> containsTestArgs() {
        return List.of(
                Arguments.arguments("a", "b", "c", "b", true),
                Arguments.arguments("a", "b", "c", "d", false)
        );
    }

    @ParameterizedTest
    @MethodSource("containsTestArgs")
    void containsTest(String value1, String value2, String value3, String testValue, boolean expected) {

        cut.clear();
        cut.add(value1);
        cut.add(value2);
        cut.add(value3);

        boolean actual = cut.contains(testValue);

        Assertions.assertEquals(expected, actual);

    }

    static List<Arguments> setTestArgs() {
        return List.of(
                Arguments.arguments(1, "a", true),
                Arguments.arguments(10, "h", true),
                Arguments.arguments(0, "j", false),
                Arguments.arguments(11, "z", false),
                Arguments.arguments(-1, "w", false)
        );
    }

    @ParameterizedTest
    @MethodSource("setTestArgs")
    void setTest(int index, String value, boolean expected) {

        cut = new LList1();

        boolean actual = cut.set(index, value);

        Assertions.assertEquals(expected, actual);

    }

    static List<Arguments> toArrayTestArgs() {
        return List.of(
                Arguments.arguments(new String [] {"a", "b", "c"}, true),
                Arguments.arguments(new String [] {"c", "b", "a"}, false)
        );
    }

    @ParameterizedTest
    @MethodSource("toArrayTestArgs")
    void toArrayTest(String [] expectedArray, boolean expected) {

        cut.clear();
        cut.add("a");
        cut.add("b");
        cut.add("c");

        Object [] actualArray = cut.toArray();

        boolean actual = true;

        for (int i = 0; i < actualArray.length; i++) {

            if (!actualArray[i].equals(expectedArray[i])) {

                actual = false;

            }

        }

        Assertions.assertEquals(expected, actual);

    }

    @Test
    void removeAllTest() {

        boolean expected = true;

        boolean actual = cut.removeAll();

        Assertions.assertEquals(expected, actual);

    }

    @Test
    void isEmptyTest() {

        cut.clear();

        boolean expected = true, actual = cut.isEmpty();

        Assertions.assertEquals(expected, actual);

    }

    @Test
    void isNotEmptyTest() {

        boolean expected = false, actual = cut.isEmpty();

        Assertions.assertEquals(expected, actual);

    }

}

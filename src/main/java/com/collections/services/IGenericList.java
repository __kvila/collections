package main.java.com.collections.services;

public interface IGenericList<T> {

    void clear();

    int size();

     T get(int index);

    boolean add(T value);

    boolean add(int index, T value);

    int remove(T value);

    int removeByIndex(int index);

    boolean contains(T value);

    boolean set(int index, T value);

    void print();

    T[] toArray();

    boolean removeAll();

}

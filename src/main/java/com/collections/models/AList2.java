package main.java.com.collections.models;

import main.java.com.collections.services.IGenericList;

public class AList2 <T> implements IGenericList <T> {

    Object [] arrayList;

    public AList2() {

        this.arrayList = (T[]) new Object[10];
    }

    public AList2(int capacity) {

        this.arrayList = (T[]) new Object[capacity];
    }

    public AList2(T[] array) {
        this.arrayList = array;
    }

    @Override
    public void clear() {
        for (int i = 0; i < arrayList.length; i++) {
            arrayList[i] = null;
        }
    }

    @Override
    public int size() {
        return arrayList.length;
    }

    @Override
    public T get(int index) {
        if (index >= arrayList.length || index < 0) {
            System.out.println("Array list have no element on this index.");
            return null;
        } else {
            return (T) arrayList[index];
        }
    }

    @Override
    public boolean add(T value) {

        T [] tempArray = (T[]) new Object[arrayList.length + 1];
        int size = this.arrayList.length;

        for (int i = 0; i < tempArray.length - 1; i++) {
            tempArray[i] = (T) arrayList[i];
        }
        tempArray[tempArray.length - 1] = value;

        arrayList = tempArray;

        if (arrayList.length > size && arrayList[arrayList.length - 1] == value) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean add(int index, T value) {

        if (index >= arrayList.length || index < 0) {

            return false;

        }

        T [] tempArray = (T[]) new Object[arrayList.length + 1];
        int size = this.arrayList.length;

        for (int i = 0, j = 0; j < arrayList.length; i++, j++) {

            if (i == index) {

                tempArray[i] = value;
                j--;
                continue;

            }

            tempArray[i] = (T) arrayList[j];
        }

        arrayList = tempArray;

        if (arrayList.length > size && arrayList[index] == value) {
            return true;
        } else {
            return false;
        }

    }

    @Override
    public int remove(T value) {

        T [] tempArray = (T[]) new Object[arrayList.length];

        for (int i = 0; i < arrayList.length; i++) {
            if (arrayList[i].equals(value)) {

                tempArray = (T[]) new Object[arrayList.length - 1];
                break;
            }
        }

        for (int i = 0, j = 0; i < arrayList.length; i++, j++) {

            if (arrayList[i].equals(value)) {

                for (int k = i + 1; k < arrayList.length; k++, j++) {
                    tempArray[j] = (T) arrayList[k];
                }
                arrayList = tempArray;
                return 1;
            }
            tempArray[i] = (T) arrayList[i];
        }

        System.out.println("Array have no element like that.");
        return 0;
    }

    @Override
    public int removeByIndex(int index) {

        if (index < 0 || index >= arrayList.length) {
            System.out.println("Array have no element with this index.");
            return 0;
        }

        T [] tempArray = (T[]) new Object[arrayList.length - 1];

        for (int i = 0, j = 0; i < arrayList.length; i++, j++) {
            if (i == index) {

                j--;
                continue;
            }

            tempArray[j] = (T) this.arrayList[i];
        }

        this.arrayList = tempArray;

        return 1;
    }

    @Override
    public boolean contains(T value) {

        for (int i = 0; i < arrayList.length; i++) {
            if (arrayList[i].equals(value)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean set(int index, T value) {

        if (index < 0 || index >= arrayList.length) {
            System.out.println("Array have no element with this index.");
            return false;
        }

        for (int i = 0; i < arrayList.length; i++) {

            if (i == index) {
                arrayList[i] = value;
                break;
            }
        }

        return true;
    }

    @Override
    public void print() {

        System.out.print("[");

        for (int i = 0; i < arrayList.length; i++) {
            if (i == arrayList.length - 1) {
                System.out.println(arrayList[i] + "]");
            } else {
                System.out.print(arrayList[i] + ",");
            }
        }
    }

    @Override
    public T[] toArray() {
        T [] newArrayList = (T[]) new Object[arrayList.length];

        for (int i = 0; i < newArrayList.length; i++) {
            newArrayList[i] = (T) arrayList[i];
        }

        return newArrayList;
    }

    @Override
    public boolean removeAll() {

        for (int i = 0; i < arrayList.length; i++) {
            arrayList[i] = null;

            if(arrayList[i] != null) {
                return false;
            }
        }

        return true;
    }
}

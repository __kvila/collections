package main.java.com.collections.models;

import main.java.com.collections.services.IGenericList;

public class LList1 <T> implements IGenericList <T> {

    public class Node <T> {
        T element;
        Node next;
    }

    Node head, tail;

    int size = 0;

    public LList1 () {
        for (int i = 0; i < 10; i++) {
            if (head == null) {
                Node el = new Node();
                el.element = null;
                el.next = null;
                tail = head = el;
                size++;
            } else {
                Node el = new Node();
                el.element = null;
                el.next = null;
                tail.next = el;
                tail = el;
                size++;
            }
        }
    }

    public LList1 (int capacity) {
        for (int i = 0; i < capacity; i++) {
            if (head == null) {
                Node el = new Node();
                el.element = null;
                el.next = null;
                tail = head = el;
                size++;
            } else {
                Node el = new Node();
                el.element = null;
                el.next = null;
                tail.next = el;
                tail = el;
                size++;
            }
        }
    }

    public LList1 (LList1 list) {

        Node temp = list.head;

        while (temp != null) {
            if (head == null) {
                Node el = new Node();
                el.element = temp.element;
                el.next = null;
                tail = head = el;
                size++;
            } else {
                Node el = new Node();
                el.element = temp.element;
                el.next = null;
                tail.next = el;
                tail = el;
                size++;
            }

            temp = temp.next;
        }
    }

    @Override
    public void clear() {
        while (head != null) {
            head = head.next;
            size--;
        }
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public T get(int index) {

        if(index > size || index <= 0) {
            System.out.println("Element with this index doesn't exist.");
            return null;
        }

        Node temp = head;
        int count = 1;

        while (temp != null) {

            if (count == index) {
                return (T) temp.element;
            }

            temp = temp.next;
            count++;
        }

        return null;
    }

    @Override
    public boolean add(T value) {

        Node el = new Node();
        el.element = value;
        el.next = null;

        if (head == null) {
            head = el;
            tail = el;
        } else {
            tail.next = el;
            tail = el;
        }

        if (tail.element != value) {
            return false;
        }

        size++;

        return true;
    }

    @Override
    public boolean add(int index, T value) {
        if(index > size || index <= 0) {
            System.out.println("Element with this index doesn't exist.");
            return false;
        }

        if(index == 1) {
            Node el = new Node();
            el.element = value;
            el.next = head.next;
            head = el;

            size++;
            return true;
        }

        Node temp = head;
        int count = 1;

        while (temp != null) {
            if (count + 1 == index) {
                Node el = new Node();
                el.element = value;
                el.next = temp.next;
                temp.next = el;

                if (el.next == null) {
                    tail.next = el;
                    tail = el;
                }

                size++;
                return true;
            }

            temp = temp.next;
            count++;
        }

        return false;
    }

    @Override
    public int remove(T value) {

        if(isEmpty()) {
            return 0;
        }

        if (head.element == value) {
            head = head.next;
            size--;

            return 1;
        }

        Node temp = head;

        if (temp.next != null) {
            while (temp != null) {

                if (temp.next.element == value) {
                    temp.next = temp.next.next;

                    if (temp.next == null) {
                        tail = temp;
                    }

                    size--;
                    return 1;
                }

                if (temp.next.element != value && temp.next.next == null) {
                    System.out.println("Element with this value doesn't exist!");
                    break;
                }

                temp = temp.next;
            }
        } else {

            return 0;

        }

        return 0;
    }

    @Override
    public int removeByIndex(int index) {

        if(index > size || index <= 0) {
            System.out.println("Element with this index doesn't exist.");
            return 0;
        }

        if(index == 1) {

            head = head.next;

            size--;
            return 1;
        }

        Node temp = head;
        int count = 1;

        while (temp != null) {
            if (count + 1 == index) {

                temp.next = temp.next.next;

                if (temp.next == null) {
                    tail = temp;
                }

                size--;
                return 1;
            }

            temp = temp.next;
            count++;
        }

        return 0;
    }

    @Override
    public boolean contains(T value) {


        if (isEmpty()) {
            return false;
        }

        Node temp = head;

        while (temp != null) {

            if (temp.element == value) {
                return true;
            }

            temp = temp.next;
        }

        return false;
    }

    @Override
    public boolean set(int index, T value) {

        if(index > size || index <= 0) {
            System.out.println("Element with this index doesn't exist.");
            return false;
        }

        Node temp = head;
        int count = 1;

        while (temp != null) {

            if(count == index) {

                temp.element = value;
                return true;
            }

            temp = temp.next;
            count++;
        }

        return false;
    }

    @Override
    public void print() {

        if(isEmpty()) {
            return;
        }

        Node temp = head;
        while (temp != null) {
            System.out.print(temp.element + " ");
            temp = temp.next;
        }
        System.out.println();
    }

    @Override
    public T[] toArray() {

        Object [] array = new Object[this.size];

        if (isEmpty()) {
            return (T[]) array;
        }

        Node temp = head;
        int i = 0;

        while (temp != null) {
            array[i] =  temp.element;
            temp = temp.next;
            i++;
        }

        return (T[]) array;
    }

    @Override
    public boolean removeAll() {

        while (head != null) {
            head = head.next;
            size--;
        }

        return head == null;
    }

    public boolean isEmpty() {
        if (head == null) {
            System.out.println("List is empty");
            return true;
        } else {
            return false;
        }
    }
}

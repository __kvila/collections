package main.java.com.collections.models;

import main.java.com.collections.services.IList;

public class AList1 implements IList {

    int [] arrayList;

    public AList1() {
        this.arrayList = new int[10];
    }

    public AList1(int capacity) {
        this.arrayList = new int[capacity];
    }

    public AList1(int[] array) {
        this.arrayList = array;
    }

    @Override
    public void clear() {
        for (int i = 0; i < arrayList.length; i++) {
            arrayList[i] = 0;
        }
    }

    @Override
    public int size() {
        return arrayList.length;
    }

    @Override
    public int get(int index) {
        if (index >= arrayList.length || index < 0) {
            System.out.println("Array list have no element on this index.");
            return 0;
        } else {
            return arrayList[index];
        }
    }

    @Override
    public boolean add(int value) {

        int [] tempArray = new int[this.arrayList.length + 1];
        int size = this.arrayList.length;

        for (int i = 0; i < tempArray.length - 1; i++) {
            tempArray[i] = arrayList[i];
        }

        tempArray[tempArray.length - 1] = value;

        arrayList = tempArray;

        if (arrayList.length > size && arrayList[arrayList.length - 1] == value) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean add(int index, int value) {

        int [] tempArray = new int[this.arrayList.length + 1];
        int size = this.arrayList.length;

        for (int i = 0, j = 0; j < arrayList.length; i++, j++) {

            if (i == index) {

                tempArray[i] = value;

                j--;

                continue;

            }

            tempArray[i] = arrayList[j];
        }

        arrayList = tempArray;

        if (arrayList.length > size && arrayList[index] == value) {
            return true;
        } else {
            return false;
        }

    }

    @Override
    public int remove(int number) {

        int [] tempArray = {};

        for (int element : arrayList) {

            if (element == number) {

                tempArray = new int[this.arrayList.length - 1];
                break;
            }
        }

            for (int i = 0, j = 0; i < arrayList.length; i++, j++) {

                if (arrayList[i] == number) {

                    for (int k = i + 1; k < arrayList.length; k++, j++) {
                        tempArray[j] = arrayList[k];
                    }
                    arrayList = tempArray;
                    return 1;
                }
            }

        return 0;
    }

    @Override
    public int removeByIndex(int index) {

        if (index < 0 || index >= arrayList.length) {
            System.out.println("Array have no element with this index.");
            return 0;
        }

        int [] tempArray = new int[arrayList.length - 1];

        for (int i = 0, j = 0; i < arrayList.length; i++, j++) {
            if (i == index) {

                j--;
                continue;
            }

            tempArray[j] = this.arrayList[i];
        }

        this.arrayList = tempArray;

        return 1;
    }

    @Override
    public boolean contains(int value) {

        for (int element : arrayList) {
            if (element == value) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean set(int index, int value) {

        if (index < 0 || index >= arrayList.length) {
            System.out.println("Array have no element with this index.");
            return false;
        }

        for (int i = 0; i < arrayList.length; i++) {

            if (i == index) {
                arrayList[i] = value;
                break;
            }
        }

        return true;
    }

    @Override
    public void print() {

        System.out.print("[");

        for (int i = 0; i < arrayList.length; i++) {
            if (i == arrayList.length - 1) {
                System.out.println(arrayList[i] + "]");
            } else {
                System.out.print(arrayList[i] + ",");
            }
        }
    }

    @Override
    public int[] toArray() {
        int [] newArrayList = new int[arrayList.length];

        for (int i = 0; i < newArrayList.length; i++) {
            newArrayList[i] = arrayList[i];
        }

        return newArrayList;
    }

    @Override
    public boolean removeAll(int[] ar) {

        for (int i = 0; i < ar.length; i++) {

            ar[i] = 0;

            if(ar[i] != 0) {
                return false;
            }
        }

        return true;
    }
}

package main.java.com.collections;

import main.java.com.collections.models.AList1;
import main.java.com.collections.models.AList2;
import main.java.com.collections.models.LList1;
import main.java.com.collections.models.LList2;

import java.util.LinkedList;


public class Main {
    public static void main(String[] args) {
        LList2 list = new LList2();

        Object [] arr;

        list.print();

        System.out.println(list.size());

        list.add(5);

        list.print();

        System.out.println(list.add(5, 15));

        list.print();

        list.remove(15);

        list.print();

        LList2 list2 = new LList2(list);

        list2.print();

        LinkedList<Integer> l = new LinkedList<>();

        l.add(5);
        l.add(15);
        l.add(25);

        l.removeAll(l);

        System.out.println(l);

    }
}
